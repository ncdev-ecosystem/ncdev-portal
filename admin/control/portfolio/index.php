<?php
require_once('conf3456.php');

//Email Validation
function check_email_address($email) {
  // First, we check that there's one @ symbol, and that the lengths are right
  if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
    // Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
    return false;
  }
  // Split it into sections to make life easier
  $email_array = explode("@", $email);
  $local_array = explode(".", $email_array[0]);
  for ($i = 0; $i < sizeof($local_array); $i++) {
     if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) {
      return false;
    }
  }  
  if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) { // Check if domain is IP. If not, it should be valid domain name
    $domain_array = explode(".", $email_array[1]);
    if (sizeof($domain_array) < 2) {
        return false; // Not enough parts to domain
    }
    for ($i = 0; $i < sizeof($domain_array); $i++) {
      if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) {
        return false;
      }
    }
  }
  return true;
}
?>﻿
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="This is the work/activities done by the NCDev Ecosystem society.">
	<meta name"keyword" content="North Cape, Geeks, Hack, Hackathon, Geekulcha, SPU, DeDaT, Innovation, Localization">
    <meta name="author" content="Nomfundo Nkosi, Keenan Gwells, Thato Duiker, Tiyani Nghonyama">

    <title>NCDev Portfolio</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/agency.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
   <!-- favicon -->	
	<link rel="shortcut icon" href="http://ncdev.co.za/assets/img/favicon.ico" type="image/x-icon">
	<link rel="icon" href="http://www.ncdev.co.za/assets/img/favicon.ico" type="image/x-icon">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->



</head>

<body id="page-top" class="index" >
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-51ea2ff677aa9864" async="async"></script>

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" style="background: #F41F19;">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a style="background: #F41F19;" class="navbar-brand page-scroll" href="http://www.ncdev.co.za"><img src="../../../../assets/img/Backup.png" width="100px" alt="" width="100px" ></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="background: #F41F19;">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                     <h2 style="color: #fff;">Portfolio Management</h2>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

   

    <!-- Portfolio Grid Section -->
    <section id="contact" class="" style="background: #CC0E0E;">
        <div class="container">
            
            <div class="row">
<?php 


if (!empty($_POST['submit']) && isset($_POST['submit'])) {

$title = mysqli_real_escape_string($mysqli, $_POST['title']); 
					 $type = mysqli_real_escape_string($mysqli, $_POST['ptype']); 
					 $img = mysqli_real_escape_string($mysqli, $_POST['image_url']); 
					  $link = mysqli_real_escape_string($mysqli, $_POST['item_url']); 
 $body = mysqli_real_escape_string($mysqli, $_POST['body']);
 $pb = mysqli_real_escape_string($mysqli, $_POST['posted_by']);
					  
					  
					 $checktitle = mysqli_query($mysqli, "select * from dev_portfolio where title='$title'");
	
			 
if(empty($title)) { ?>	
									<script>
										alert("Please the Title"); window.history.back();
									</script>	
				<?php
			

 }
						else if(mysqli_num_rows($checktitle) > 0) {			
		?>	
				<script>
				alert("Sorry, you have already used this title."); window.history.back();
				</script>	
				<?php }
				else if(empty($body)) {	?>
									<script>
										alert("Please enter the description");
										window.history.back();
									</script>
					 <?php	
						
	}		
						else if($type=='Select Item Type' && empty($type)) {	?>
									<script>
										alert("Please select Item Type");
										window.history.back();
									</script>
					 <?php	
					 
						}
						else {
					   
			$query = mysqli_query($mysqli, "INSERT INTO dev_portfolio (title, category, date_submitted, link, image, body, posted_by) 
			VALUES('$title', '$type', NOW(), '$link', '$img', '$body', '$pb')");

if($query) {

?>	
				<script>
				alert("Success! The portfolio item has been recorded"); window.location.href="index.php";
				</script>	
				<?php 
}
else {
?>	
				<script>
				alert("Oops! Something went wrong, please try again later."); window.history.back();
				</script>	
				<?php 
}

} 
}

else {
?>


       
         
            <div class="row">
                <div class="col-lg-12">
                    <form method="post" action="">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="* Title" name="title" id="name" required="" data-validation-required-message="Please enter title of the item." aria-invalid="false">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="image_url" placeholder="* Image URL" id="email" required="" data-validation-required-message="Please enter the Image URL." aria-invalid="false">
                                    <p class="help-block text-danger"></p>
                                </div>
 <div class="form-group">
                                    <input type="text" class="form-control" name="item_url" placeholder="Item URL" id="email" required="" data-validation-required-message="Please enter the Item URL." aria-invalid="false">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="post_by" class="form-control" placeholder="Posted By" id="phone" aria-invalid="false">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
 <div class="form-group">
<select name="ptype" class="form-control">
<option>* Select Item Type</option>
<option>Meet-Up</option>
<option>Hackathon</option>
<option>Training</option>
<option>Innovation</option>
<option>Outreach</option>
<option>Activation</option>
</select>                                   

                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" name="body" placeholder="* Description" id="message" required="" data-validation-required-message="Please enter the description."></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center">
                             
                                <input type="submit" name="submit" class="btn btn-xl" value="Submit Item"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

<?php
}
?>
 
              </div>  
            </div>
        
    </section>

    

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; <a href="http://www.ncdev.co.za">NCDev Ecosystem</a> 2015</span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li><a href="https://twitter.com/NCDevEco"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="https://www.facebook.com/pages/NCDev-Ecosystem/1596329860610514"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="info@ncdev.co.za"><i class="fa fa-envelope-o"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">
                        <li>A platform by <a href="http://geekulcha.com">Geekulcha</a>.
                        </li>
                      
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <!-- Portfolio Modals -->
    <!-- Use the modals below to showcase details about your portfolio projects! -->

    <!-- Portfolio Modal 1 -->
    <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>Geeks Connect-SPU</h2>
                            <p class="item-intro text-muted">Geekulcha expanding tech student body to Northern Cape</p>
                            <img class="img-responsive img-centered" src="https://c1.staticflickr.com/9/8726/16822195348_38b74e8769.jpg" alt="">
                            <p>
Geekulcha is a community of like minded youngsters in IT who collaborate in an effort to share opportunities and
learn from one another, and regular readers of this site will know that we’ve worked extensively with the Pretoria-based
 organisation over the last couple of years.</p>

<p>It seems like Guateng is no longer big enough to contain their
ambition, and the network is currently expanding to a permanent presence at Sol Plaatje University (SPU).
 Geekulcha plans to ‘’make Northern Cape alive and a strong pillar of the ICT world,’’
 says organiser Tiyani Nghonyama.</p>

<p>Sol Plaatje University is not wholly unfamiliar territory for Geekulcha.
It’s organised an electroMobile challenge there in the past and gave away an Arduino set to the best local innovative idea.
 Nghonyama says 50 students are now part of their new project which is aptly named ‘’Geek Connect’’
 and describes their task at SPU as ‘’making the student developer ecosystem vibrant and making them (students) embrace maker culture”.</p>

<p>mLab Southern Africa, Sol Plaatje University, the department of Science and Technology and the provincial Economic Development and Tourism Department
in the Northern Cape are partners on this Geekulcha project.
In addition, Geekulcha will also appoint three ambassadors on campus at SPU. Students are encouraged to be on the look out for technology and entrepreneurship meetups in Kimberly starting this month. Geekulcha also promises opportunities for travel to Pretoria for students in order to give students varied experiences.
by Matshelane Mamabolo</p>
                            <p>
                                <strong>For more information visit </strong> <a href="http://www.geekulcha.com /1">Geekulcha</a></p>

                            <ul class="list-inline">
                                <li>Date: April 2015</li>
                                <li>Client: Sol Plaatje University</li>
                                <li>Category: Workshop</li>
                            </ul>
                            <div>
                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 </div>
    <!-- Portfolio Modal 2 -->
    <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <h2>KIMBERLEY DIAMOND CUP</h2>
                            <p class="item-intro text-muted">Skateboarding Event - Sports Science</p>
                            <img class="img-responsive img-centered" src="http://4.bp.blogspot.com/-rKIutdpidaQ/VUOs9-XGCPI/AAAAAAAAEaA/Xj4UbzQCtQU/s1600/2015-04-18%2B10.31.06.jpg" alt="">
                            <p>Sports Science Workshop at KDC</p>


<p>Geekulcha Electronics added a bit of “geek cool” to the Skateboarding for Hope, Kimberley Diamond Cup regional qualifier.

Geekulcha was tasked with conducting a sports science workshop during the skateboarding event which was attended by children from local schools and orphanages.
“The Geekulcha team shared knowledge about the sports science ecosystem and how cloud computing is enabling brand new possibilities in the world of sports and skateboarding,”
according to Geekulcha’s COO Tiyani Nghonyama.
“Skaters were also introduced to technologies such as Arduino, Raspberry Pi and the Intel Galileo boards. We also hacked a skateboard with sensor tech to
detect falls with audio and bright LED lights.”</p>

<p>Geekulcha’s three new student ambassadors from Sol Plaatje University were also at the event. 
The three will run GeeksConnect (mobile and web meet-ups), Electronics Wednesdays and the recently launched Raeketsetsa (ICT girls initiative) in and around SPU.
Nghonyama says the ultimate goal of Geekulcha in Northern Cape is to establish an mLab in Kimberley.

by Matshelane Mamabolo</p>
                            <p>For more information <a href="http://ncdev.co.za/">NCDev Ecosystem</a>.</p>
                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Portfolio Modal 3 -->
    <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>GEEKULCHA HACKATHON</h2>
                            <p class="item-intro text-muted">Geekulcha Hackathon</p>
                            <img class="img-responsive img-centered" src="https://c1.staticflickr.com/1/465/19391921620_6a22a255d1.jpg" alt="">
     <p>It was razzmatazz, indeed it was. The Geeks were out in numbers to 
attend and participate in this year’s Geekulcha Hackathon with the aim to provide business and social solutions using technical skills. Geekulcha was sponsored by the likes of T-Systems, City of Tshwane, Intel, IBM, Redbull, Standard bank, mLab Southern Africa, Northern Cape Department Economic Development and Tourism, Kalapeng Pharmacies, Empire State, Google Developer Group Pretoria, FABU Health, Communica, and Build.</p>
<p>
It was during the freezing weekend of the 26th to 28th in Midrand, at 
T-Systems South Africa, where computer programming was the name of game. The Geeks, as they call themselves, were using collaborative IT efforts to bring solutions brought forward by the sponsors with skills ranging from programmers, designers, robotics and input from aspiring entrepreneurs. The participants were from universities and high schools.</p>
<p>
The attendance of females at tech events is on the rise. It is important to realize that hackathons are not limited to gents. The Geeks had an abundance of fun. There was a variety of expertise and it was a great networking opportunity. There was a PlayStation for breaks, good music playing in the background and the food was AMAZING. The purpose of this hackathon was to consider not only the delivery of a working solution but considering the commerciality of the solution being provided. Geekulcha aims to instil an entrepreneurial mind in the youth and develop both business and IT skills.
</p><p>
On Friday night, the Geeks separated into groups and selected their projects which were later pitched to all participants. The main objective of the projects was to have social and business impact. Projects ranged from health, vehicle safety, logistics, gambling, and electronics to assist farming communities etc.
</p><p>
On Saturday, different speakers shared insightful topics and apps with the Geeks.</p>
<p>
Sunday was an exciting day as it is the time everyone was waiting for, presentation time. The Geeks were working on 21 different projects. A panel of judges consisting of various organizations and sponsors judged the projects on Innovation, impact (social and economical), market potential and scalability of the project.
</p><p>
The winning team addressed the challenge of getting people to clean their environment. They used platforms such as IoT in marking their smart bin much smarter. The runners up were close behind addressing a smart car that alerts nearest hospitals in cases of accidents through the utilization of IoT Technologies and electronics sensors.
</p><p>
The prizes won are as follows:</p>
<p>
- Smart Bin (R3500 shopping voucher from Geekulcha, opportunity to pitch at mLab and speak at Tech4Africa)<br>

- Mobi Electro (Exhibition at Tech4Africa and R1000 voucher from GDG Pretoria)<br>

- Synergy and Handi Connect (R500 voucher from GDG Pretoria and Geekulcha)<br><br>

* GeekIn project to be adapted at the next Geekulcha hackathon taking place TUT 25-26 July<br><br>

Ultimate GeekStar: Neo from Handi Connect</p>
<p>
The Geekulcha Hackathon 2015 was made possible by the great support Geekulcha received by all the partners and sponsors. Mixo thanked the sponsors for showing enormous trust and faith in the work Geekulcha is doing.
</p><p>
" You can't ignore the Geeks forever " #‎iBlameGK‬ #GKHack2015 #HireGeeks‬</p>
                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

   
            

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>


    <!-- Custom Theme JavaScript -->
    <script src="js/agency.js"></script>

</body>

</html>